import serial
import time
import sys

b = bytearray()
b.extend(map(ord, "ST"))

stm32 = serial.Serial('COM5', 128000)

while stm32.in_waiting != 0:
    stm32.read(stm32.in_waiting)

print("Flushed buffer")

stm32.write(b)

print("Waiting for size from micrcontroller")

while stm32.in_waiting == 0:
    pass

print("Size received from microcontroller")

size = int.from_bytes(stm32.read(2), byteorder='little', signed=False)

print("Waiting for ", size, " bytes from microcontroller")

image = bytearray()
read_bytes = 0

while read_bytes != size:
    txsz = stm32.in_waiting
    print("Transfer Size: ", txsz)
    read_bytes += txsz
    image.extend(stm32.read(txsz))
    print("Current size: ", len(image))
    time.sleep(0.5)  

print("Received ", read_bytes, " bytes from microcontorller")

print("Writing to file")

print(len(image), " bytes to be written to file out of ", read_bytes, " bytes")

if len(sys.argv) < 2:
    filename = "image.jpeg"
else:
    filename = sys.argv[1]

f = open(filename, 'wb')

f.write(image)
f.close()
stm32.close()