/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

//#include "ch.h"
#include "hal.h"
#include "hal_usbh.h"
#include "usbh/dev/uvc.h"
#include "string.h"
#include "usart.h"
#include "gpio.h"

extern USBHDriver USBHD1;
extern USBHUVCDriver USBHUVCD[HAL_USBHUVC_MAX_INSTANCES];


// Storage classes for generic frame and format descriptors (since different formats have different descriptors)
typedef struct
{
	uint16_t width;
	uint16_t height;
	uint8_t index;
}videoFrame;

typedef struct
{
	uint8_t bitsPerPixel;
	uint8_t defaultFrameIndex;
	uint8_t formatIndex;
	uint8_t numFrames;
	uint8_t guidFormat[16];
	uint8_t subtype;
	videoFrame frames[10];
}videoFormat;

// Storage for format and frame data
videoFormat availableFormats[5];
uint8_t numFormats = 0;
uint8_t* vs_start = 0x00000000;

// Loops through all the frames that that a format reports, adding them to the global lists
static int getVideoFrames(uint8_t* ptr, uint8_t formatIndex)
{
	uint8_t current_frame = 0;
	uint8_t* ptr_to_frame = ptr + ptr[0];
	
	while(current_frame < availableFormats[formatIndex].numFrames)
	{
		availableFormats[formatIndex].frames[current_frame].width = ptr_to_frame[5] | (ptr_to_frame[6] << 8);
		availableFormats[formatIndex].frames[current_frame].height = ptr_to_frame[7] | (ptr_to_frame[8] << 8);
		availableFormats[formatIndex].frames[current_frame].index = ptr_to_frame[3];
		
		ptr_to_frame += ptr_to_frame[0];
		current_frame++;
	}
	
	
	
	return ptr_to_frame - ptr;
}

// Finds all the format descriptors reported by the camera and fills the global list with their data(could be changed to get passed a pointer to the list as well)
static void getVideoFormats(USBHDriver* host)
{
	uint8_t* current = 0x00;
	current = host->rootport.device.fullConfigurationDescriptor;
	uint8_t* start = current;
	uint16_t total_length = host->rootport.device.basicConfigDesc.wTotalLength;
	uint8_t bDescriptorType;
	uint8_t bDescriptorSubtype;
	uint16_t vs_length = 0x00;
	
	// Find the videostreaming interface descriptor and save the length and start address of it
	while((current - start) < total_length)
	{
		uint8_t bDescriptorType = current[1];
		uint8_t bInterfaceClass = current[5];
		uint8_t bInterfaceSubClass = current[6];
		if(bDescriptorType == USBH_DT_INTERFACE && bInterfaceClass == UVC_CC_VIDEO && bInterfaceSubClass == UVC_SC_VIDEOSTREAMING)
		{
			vs_start = current;
			current += current[0];
			vs_length = current[4] | (current[5] << 8);
			break;
		}
		current += current[0];
	}
	
	// Loop over the descriptors under the vs interface, adding all the formats found to the global lists
	while((current - vs_start) < vs_length)
	{
		bDescriptorType = current[1];
		bDescriptorSubtype = current[2];
		
		if(bDescriptorType == UVC_CS_INTERFACE)
		{
			switch(bDescriptorSubtype)
			{
				case UVC_VS_FORMAT_FRAME_BASED:
					availableFormats[numFormats].formatIndex = current[3];
					availableFormats[numFormats].numFrames = current[4];
					availableFormats[numFormats].defaultFrameIndex = current[22];
					availableFormats[numFormats].subtype = 0x10;
					
					memcpy(availableFormats[numFormats].guidFormat, &current[5], 16);
					memset(availableFormats[numFormats].frames, 0, sizeof(videoFrame) * 10);
					current += getVideoFrames(current, numFormats);
					numFormats++;
					break;
				case UVC_VS_FORMAT_UNCOMPRESSED:
					availableFormats[numFormats].formatIndex = current[3];
					availableFormats[numFormats].numFrames = current[4];
					availableFormats[numFormats].defaultFrameIndex = current[22];
					availableFormats[numFormats].subtype = 0x04;
				
					memcpy(availableFormats[numFormats].guidFormat, &current[5], 16);
					memset(availableFormats[numFormats].frames, 0, sizeof(videoFrame) * 10);
					current += getVideoFrames(current, numFormats);
					numFormats++;
					break;
				case UVC_VS_FORMAT_MJPEG:
					availableFormats[numFormats].formatIndex = current[3];
					availableFormats[numFormats].numFrames = current[4];
					availableFormats[numFormats].defaultFrameIndex = current[6];
					availableFormats[numFormats].subtype = 0x06;
					memset(availableFormats[numFormats].guidFormat, '\0', 16);
					strcpy((char*)availableFormats[numFormats].guidFormat,"MJPEG");
					memset(availableFormats[numFormats].frames, 0, sizeof(videoFrame) * 10);
					current += getVideoFrames(current, numFormats);
					numFormats++;
					break;
				default:
					current += current[0];
					break;
			}
		}
	}
	
}

// Probe the camera and commit sane settings
static void probeAndCommit(USBHDriver* host, USBHUVCDriver* driver)
{
	uint16_t maxWidth = 0x00;
	uint16_t maxHeight = 0x00;
	uint8_t frame_num = 0x00;
	uint8_t best_frame = 0xFF;
	uint8_t best_format = 0xFF;
	
	// Find the first JPEG format and store it's index
	for(uint8_t formatIndex = 0; formatIndex < 5; formatIndex++)
	{
		if(availableFormats[formatIndex].subtype == UVC_VS_FORMAT_MJPEG)
			best_format = formatIndex;
	}
	
	// Under that format, find the highest resolution frame and save it's index
	while(frame_num < availableFormats[best_format].numFrames)
	{		
		uint16_t w = availableFormats[best_format].frames[frame_num].width;
		uint16_t h = availableFormats[best_format].frames[frame_num].height;
		
		if(w > maxWidth && h > maxHeight)
		{
			maxWidth = w;
			maxHeight = h;
			best_frame = availableFormats[best_format].frames[frame_num].index;
		}
		
		frame_num++;
	}
	
	if(best_frame == 0xFF)
		return; // TODO: Do some error handling
	
	// Fill out the probe structure with the values gotten
	driver[0].pc.bFormatIndex = availableFormats[best_format].formatIndex;
	driver[0].pc.bFrameIndex = best_frame;
	//driver[0].pc.dwFrameInterval = 5000000; // Frame interval in 100ns units (5000000 = 0.5s per frame)
	usbhuvcProbe(driver);
	//driver[0].pc.dwFrameInterval = driver[0].pc_max.dwFrameInterval; // Just use the highest framerate the camera will let us
	driver[0].pc = driver[0].pc_max; // Use the highest settings for everything given the format and frame specified
	usbhuvcCommit(driver);
	return;
}

extern uint8_t uvc_framebuffer[USBH_UVC_MAX_FRAMEBUFFER_SIZE];
extern uint16_t frame_size;
extern uint16_t frame_count;
extern UART_HandleTypeDef huart2;

// To pass the pclk values to the UART driver
const uint32_t pclk2 = STM32_PCLK2;
const uint32_t pclk1 = STM32_PCLK1;

/*
 * Application entry point.
 */

union word_array{
	uint16_t word;
	uint8_t buffer[2];
} rx_buffer;

enum main_state
{
	STATE_INIT,              // Initialize video format information
	STATE_WAIT_FOR_TRIGGER,  // Wait for trigger over UART(blocking cause I couldn't get the non-blocking to work)
	STATE_CAMERA_INIT,       // Initialize the camera with probe/commit and open the iso endpoint
	STATE_CAMERA_RECEIVE,    // Wait until you get a couple frames from the camera(so that you can get a stable picture)
	STATE_TRANSFER           // Transfer image over uart
}state;

int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
	HAL_Init();
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	osalSysEnable();
  usbhInit();
	usbhStart(&USBHD1);
	
	state = STATE_INIT;
	

	/*
	 * Main loop is a FSM just for handling the camera
	 */
	
  while (true) {
    usbhMainLoop(&USBHD1);
		switch(state)
		{
			case STATE_INIT:
				if(usbhuvcGetState(USBHUVCD) == USBHUVC_STATE_ACTIVE)
				{
					getVideoFormats(&USBHD1);
					state = STATE_WAIT_FOR_TRIGGER;
				}
				break;
			case STATE_WAIT_FOR_TRIGGER:
				// Blocking wait for trigger command("ST") over UART
				HAL_UART_Receive(&huart2, (uint8_t*)&rx_buffer, 2, 0xFFFFFFFF);
				if(rx_buffer.word == 0x5453)
				{
					state = STATE_CAMERA_INIT;
				}
				else
				{
					rx_buffer.word = 0x0000;
				}
				break;
			case STATE_CAMERA_INIT:
				// Initialize the camera with a probe and commit and start the video stream
				if(usbhuvcGetState(USBHUVCD) == USBHUVC_STATE_ACTIVE || usbhuvcGetState(USBHUVCD) == USBHUVC_STATE_READY)
				{
					probeAndCommit(&USBHD1, USBHUVCD);
					usbhuvcStreamStart(USBHUVCD, 800);
					state = STATE_CAMERA_RECEIVE;
				}
				break;
			case STATE_CAMERA_RECEIVE:
				// Get a couple frames to get a stable picture, then stop the stream/close the endpoint
				if(frame_count == 10)
				{
					frame_count = 0;
					usbhuvcStreamStop(USBHUVCD);
					state = STATE_TRANSFER;
				}
				break;
			case STATE_TRANSFER:
				// Transmit the image over the UART connection and reset the trigger buffer
				rx_buffer.word = 0x0000;
				HAL_UART_Transmit(&huart2, (uint8_t*)&frame_size, 2, 0xFFFFFFFF);
				HAL_UART_Transmit(&huart2, uvc_framebuffer, frame_size, 0xFFFFFFFF);
				state = STATE_WAIT_FOR_TRIGGER;
				break;
			default:
				state = STATE_WAIT_FOR_TRIGGER;
				break;
		}
  }
}
