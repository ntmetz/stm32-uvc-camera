# UVC Camera Running on STM32F411 Microcontroller

## Warning

Even though this projcet uses keil it only compiles under the gcc compiler because of how parts of the chibiOS HAL are written, so to compile you need to download the [arm gnu toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads "ARM GNU Download") 

## Directories

ChibiOS: contains the HAL library for the usbh host(used under Apache2 license)

F411_NUCLEO: contains board specific defines, can be modified for use with different microcontrollers/boards(also under the apache2 license)

ChibiOS_UVC_Camera: contains the keil project along with a script to get the camera data over UART(contains part of arm CMSIS and ST HAL)

## Scripts

ChibiOS_UVC_Camera/readSerial.py: send the command word to the microcontroller, then waits for the data over serial and saves it to the file specified by argv[1]

* Line 8 specifies COM5 as the serial port, that needs to be changed to whatever serial port you are using

## Notes

I had to modify the usbh files since they used os-specific calls to chibiOS for passing data between fibres, the _post function under hal_usbh_uvc.c should be modified to put the data from the packets to wherever you need it

If you wanted to use this with an OS you would have to modify osal.h to implement your lock/unlock, thread sleep, and various other os-specific functions that you might need

For the board/mcu specific files(mcuconf.h, mcuconf_community.h, board.h) there are premade ones for a lot of the STM32 mcus and boards in the [chibiOS repository](http://www.chibios.org/dokuwiki/doku.php?id=chibios:downloads:start "ChibiOS Download Page") that you can copy and modify

halconf.h, halconf_community.h, and osalconf.h contain the defines for including various parts of the chibiOS HAL

pclk2 & pclk1 are defined as global variables to pass to the UART driver since it was cleaner than trying to include the ChibiOS HAL files in the ST HAL

Whenever the video stream is stopped with usbhuvcStreamStop you have to probe and commit the camera again before you can start the stream again, which might be a quirk of the logitech camera I used instead of a bug in the UVC driver

Because webcams are made to stream video, after a stream is started you have to wait a couple frames for the camera's to adjust it's focus/balance before you get a good picture

Basically all the structs and defines on the usb side are directly from the [class specification documents](https://www.usb.org/document-library/video-class-v15-document-set "UVC 1.5 Documents"), so look at those if you need specifics about frames/formats